package main

// import (
// 	"fmt"
// 	"math/rand"
// 	"strings"
// )

// var words = []string{
// 	"apple",
// 	"banana",
// 	"cherry",
// 	"orange",
// 	"pear",
// 	"watermelon",
// }

// func pickWord() string {
// 	return words[rand.Intn(len(words))]
// }

// func printWord(word string, guesses []string) {
// 	for _, letter := range word {
// 		if contains(guesses, string(letter)) {
// 			fmt.Printf("%c ", letter)
// 		} else {
// 			fmt.Print("_ ")
// 		}
// 	}
// 	fmt.Println()
// }

// func contains(arr []string, elem string) bool {
// 	for _, e := range arr {
// 		if e == elem {
// 			return true
// 		}
// 	}
// 	return false
// }

// func main() {
// 	// Pick a random word
// 	word := pickWord()

// 	// Initialize guesses
// 	var guesses []string

// 	// Game loop
// 	for i := 0; i < 6; i++ {
// 		// Print word
// 		printWord(word, guesses)

// 		// Get input
// 		var guess string
// 		fmt.Print("Guess a letter: ")
// 		fmt.Scanln(&guess)
// 		guess = strings.ToLower(guess)

// 		// Check input
// 		if len(guess) != 1 {
// 			fmt.Println("Please enter a single letter.")
// 			i--
// 			continue
// 		}
// 		if contains(guesses, guess) {
// 			fmt.Println("You already guessed that letter.")
// 			i--
// 			continue
// 		}

// 		// Update guesses
// 		guesses = append(guesses, guess)

// 		// Check for win
// 		if strings.Index(word, guess) != -1 {
// 			if strings.Index(strings.Join(guesses, ""), word) == -1 {
// 				fmt.Println("You win!")
// 				return
// 			}
// 		}

// 		// Check for loss
// 		if i == 5 {
// 			fmt.Println("You lose!")
// 			return
// 		}
// 	}

// }
