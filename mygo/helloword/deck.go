package main

// import (
// 	"fmt"
// 	"strings"
// )

// // to store card we creat deck
// type deck[]string

// func (d deck) print(){
// 	// _ is to inogre the number.
// 	for i, v := range d{
// 		fmt.Println(i, v)
// 	}
// }
// // reciver function.
// func newDeck() deck{
// 	dec := deck{}
// 	suits := []string{"club","spade","heart","diemond"}
// 	values := []string{"Ace","2","3","4","5","6","7","8","9","10","king","Queen","jack"}

// 	for _, s := range suits{
// 		for _, v := range values{
// 			dec = append(dec, v+" of "+s)
// 		}

// 	}
// 	return dec
// }

// // ------------reciver function-----------------
// func (d deck) deal(handsize int) (deck,deck){
// cardsInHand := d[:handsize]
// remainingCard := d[:handsize]
// return cardsInHand,remainingCard
// }

// // helper function to convert file to byte from deck to store in io file
// // we used join function to make all document into single string, if not we have to covert to string one by one.
// func (d deck) toString() string{
// 	str := strings.Join([]string(d),",")
// }

// func saveToFile(fileName string) error{
// 	str := d.toString()
// err := os.writeFile(fileName, []byte(str), 0666)
// return err
// }

// package main

import (
	"fmt"
	"math/rand"
	"os"
	"strings"
)

type deck []string

//to print list of cards
// it haas a reciever deck so therefore it should be printed using the instances of the deck type
// reciever function ---
func (d deck) print() {
	for i, v := range d {
		fmt.Println(i, v)
	}
}


// creating a newDeck
func newDeck() deck{
	cards:=deck{} //empty deck
	cardSuits :=[]string{"Spades","Club", "Diamond", "Heart"}
	cardValue := []string{"ace", "two", "three", "four", "five", "six","seven", "eight","nine", "ten", "jerk", "queen", "king"}

	for _, s :=range cardSuits{
		for _, v := range cardValue{
			cards = append(cards, v+" of "+s)
		}
	}
	return cards

}

// normal function
// func deal(d deck, handsize int) (deck, deck) {
// 	cardsInHand := d[:handsize]
// 	remainingCards := d[handsize:]
// 	return cardsInHand, remainingCards
// }

// reciever type function
func (d deck)deal(handsize int) (deck, deck) {
	cardsInHand := d[:handsize]
	remainingCards := d[handsize:]
	return cardsInHand, remainingCards
}

// helper function to convert
func (d deck) toString() string{
	return strings.Join([]string(d), ",")
	
}

func (d deck)saveToFile(fileName string) error{
	str := d.toString()
	// writefile
	err:= os.WriteFile(fileName, []byte(str), 0666)
	return err

}

func toDeck(fileName string) deck{
	s := strings.SplitAfter(string(fileName),",")
	return deck(s)
}
func newDeckFromFile(fileName string)deck {
	byteSlice, err := os.ReadFile(fileName)
	if err != nil {
		fmt.Println("Error,",err)
		os.Exit(1)
	}
	return toDeck(string(byteSlice))
	
}

//Shuffling a Deck
func(d deck) shuffle() deck{
	for i := range d{
		newIndex := rand.Intn(len(d)-1)
		d[i],d[newIndex] = d[newIndex],d[i]
	}
	return d
}