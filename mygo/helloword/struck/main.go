package main

import "fmt"

type person struct {
	Name string
	Name2 string
	contactInfo
}
type contactInfo struct{
	email string
	mobile int
}

func main() {
	// person1 := person{"Phurpa","Dorji",}
	// fmt.Println(person1)
	// var dujay person
	// fmt.Println(dujay)
	// dujay.Name,dujay.Name2 = "Phurba","Dorji"
	// fmt.Printf("%+v",dujay)
	dujay := person {

		Name:"Phurpa",
		Name2:"Dorji",
		contactInfo:contactInfo{
				email:"dujay@gmail.com",
				mobile:77613854,
		},
		
	}
	// fmt.Printf("%+v",dujay)
	// dujay.print()
	// dujayPointer := &dujay
	// dujayPointer.updateName("Pema")
	// fmt.Printf("%p",dujayPointer)
	// dujay.print()
	dujay.updateName("Tshering")
	dujay.print()
}


func (p *person) updateName(Name string){
	// (*p).Name = Name
	p.Name = Name
	
}

func (p person) print(){
	fmt.Printf("%+v",p)
}

