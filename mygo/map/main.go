package main

import "fmt"

func main(){
	// mapName = map[key type]value type{}
	 ages := map[string]string{
        "dawa":"no defination",
        "pema":"defination",
        "dorji":"let's go",
	}
	// delete(mapName,key)
	delete(ages,"dorji") // to delect we used key rather than values.
	fmt.Println((ages))
	printMap(ages)
}
// 1. Using a var keyword.
// var colors map[string]string
// 1. Using make function
// colors := make(map[string]string)

func printMap(c map[string]string){

	for ages, def := range c{
		println("defination of " + ages +  " is " + def)
	}
}
