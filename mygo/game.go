package main

import (
	"fmt"
	"math/rand"
	"strings"
)

var words = []string{"apple", "banana", "cherry", "dragonfruit", "elderberry", "fig", "grape", "honeydew", "kiwi", "lemon", "mango", "nectarine", "orange", "peach", "quince", "raspberry", "strawberry", "tangerine", "ugli", "watermelon"}

func pickWord() string {
	return words[rand.Intn(len(words))]
}

func playHangman(word string) {
	// Initialize game state
	lives := 6
	guessed := make(map[rune]bool)
	dashes := strings.Repeat("-", len(word))

	// Game loop
	for lives > 0 && dashes != word {
		// Print current status
		fmt.Printf("Word: %s\n", dashes)
		fmt.Printf("Lives: %d\n", lives)
		fmt.Printf("Guessed: %s\n", strings.Join(getGuessedLetters(guessed), ", "))

		// Get input
		var guess string
		fmt.Print("Enter a letter: ")
		fmt.Scanln(&guess)

		// Check input
		if len(guess) != 1 {
			fmt.Println("Please enter a single letter.")
			continue
		}
		guessChar := rune(guess[0])
		if guessed[guessChar] {
			fmt.Println("You already guessed that letter.")
			continue
		}

		// Update state
		guessed[guessChar] = true
		if strings.Contains(word, guess) {
			for i, c := range word {
				if c == guessChar {
					dashes = dashes[:i] + guess + dashes[i+1:]
				}
			}
		} else {
			lives--
			fmt.Printf("Sorry, %s is not in the word.\n", guess)
		}
	}

	// Print final result
	if dashes == word {
		fmt.Printf("Congratulations, you guessed the word %s!\n", word)
	} else {
		fmt.Printf("Sorry, you ran out of lives. The word was %s.\n", word)
	}
}

func getGuessedLetters(guessed map[rune]bool) []string {
	var result []string
	for letter, guessed := range guessed {
		if guessed {
			result = append(result, string(letter))
		}
	}
	return result
}

func main() {
	// Pick a random word
	word := pickWord()

	// Play Hangman
	playHangman(word)
}
